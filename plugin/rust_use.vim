if exists('g:rust_use__loaded') || &compatible
    finish
endif

let g:rust_use__loaded = 1

" Hello
let s:save_cpo = &cpo
set cpo&vim



" SET DEFAULT OPTIONS:


" BEHAVIOUR OPTIONS:

" The minimum common length before grouping two use statements:
let g:rust_use__minimum_common_path     = get(g:, 'rust_use__minimum_common_path', 2)

" Prevent useless interactive ask for common structs like HashMap, BTreeMap... because
" they have two references in the lib (e.g. std::collections::hash_map::HashMap and
" std::collections::HashMap):
let g:rust_use__detect_aliases          = get(g:, 'rust_use__detect_aliases', 1)

" If several existing use statements are candidates for adding a new item, then ask
" for the right one instead of choosing the first one:
let g:rust_use__ask_placement           = get(g:, 'rust_use__ask_placement', 1)


" STYLE:

" Style to apply when a new group is created. Possible values: 'inline', 'spaced-inline',
" 'multiline':
let g:rust_use__new_group_style         = get(g:, 'rust_use__new_group_style', 'inline')

" Add a trailing comma when a new group is created:
let g:rust_use__new_group_comma         = get(g:, 'rust_use__new_group_comma', 0)


" VIM INTERFACE OPTIONS:

" Enable custom commands (:Use, :UsePath...) in rust files:
let g:rust_use__enable_commands         = get(g:, 'rust_use__enable_commands', 1)

" Enable abbreviations for custom commands (:u, :up) in rust files:
let g:rust_use__enable_abbr             = get(g:, 'rust_use__enable_abbr', 0)

" Enable default mappings:
let g:rust_use__enable_default_mappings = get(g:, 'rust_use__enable_default_mappings', 1)

" Leader key for normal commands:
let g:rust_use__leader                  = get(g:, 'rust_use__leader', '_')


" MISC OPTIONS:

" Debug mode:
let g:rust_use__debug                   = get(g:, 'rust_use__debug', 0)




" PLUG MAPPINGS:

" Add the identifier before/under the cursor to the use statements, if possible by grouping
" it with an existing use statement:
inoremap <silent> <plug>(rust_use-use)     <esc>:call rust_use#use('<c-r><c-w>')<cr>gi
noremap  <silent> <plug>(rust_use-use)          :call rust_use#use('<c-r><c-w>')<cr>

" Add the identifier before/under the cursor to the use statements, by forcing to create
" a new use statement:
inoremap <silent> <plug>(rust_use-use_new) <esc>:call rust_use#use_new('<c-r><c-w>')<cr>gi
noremap  <silent> <plug>(rust_use-use_new)      :call rust_use#use_new('<c-r><c-w>')<cr>




" Bye
let &cpo = s:save_cpo
unlet s:save_cpo
