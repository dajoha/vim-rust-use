
"'''''''''''''''''''' function! rust_use#lib#buffer#get_range_str(range)
function! rust_use#lib#buffer#get_range_str(range)
    let line = getline(a:range.start.l)
    if a:range.start.l == a:range.end.l
        return strpart(line, a:range.start.c-1, a:range.end.c - a:range.start.c + 1)
    endif
    let lines = [ strpart(line, a:range.start.c-1) ]
    if a:range.end.l - a:range.start.l > 1
        let lines += getline(a:range.start.l+1, a:range.end.l-1)
    endif
    let line = getline(a:range.end.l)
    let lines += [ strpart(line, 0, a:range.end.c) ]

    return join(lines, "\n")
endf

"'''''''''''''''''''' function! rust_use#lib#buffer#replace_range(range, str)
function! rust_use#lib#buffer#replace_range(range, str)
    let left_to_keep = strpart(getline(a:range.start.l), 0, a:range.start.c-1)
    let right_to_keep = strpart(getline(a:range.end.l), a:range.end.c)

    let replacement_lines = split(a:str, "\n", 1)
    let replacement_lines[0] = left_to_keep . replacement_lines[0]
    let replacement_lines[-1] .= right_to_keep

    " Append or delete lines in order to fit the new number of lines:
    let nb_original_lines = a:range.end.l - a:range.start.l + 1
    let nb_replacements_lines = len(replacement_lines)
    let delta_lines = nb_replacements_lines - nb_original_lines
    if delta_lines < 0
        exe printf('%i,%i d _', a:range.start.l, a:range.start.l - delta_lines - 1)
    elseif delta_lines > 0
        call append(a:range.start.l, repeat([''], delta_lines))
    endif

    call setline(a:range.start.l, replacement_lines)
endf

"'''''''''''''''''''' function! rust_use#lib#buffer#insert_after_pos(pos, str)
function! rust_use#lib#buffer#insert_after_pos(pos, str)
    let line = getline(a:pos.l)
    let left = strpart(line, 0, a:pos.c)
    let right = strpart(line, a:pos.c)

    let new_lines = split(a:str, "\n", 1)
    let new_lines[0] = left . new_lines[0]
    let new_lines[-1] = new_lines[-1] . right

    call setline(a:pos.l, new_lines[0])
    call append(a:pos.l, new_lines[1:])
endf
