
"'''''''''''''''''''' function! rust_use#lib#commands#complete_use(ArgLead, CmdLine, CursorPos)
function! rust_use#lib#commands#complete_use(ArgLead, CmdLine, CursorPos)
	return join(rust_use#lib#std_items#get_names(), "\n")
endf

"'''''''''''''''''''' function! rust_use#lib#commands#use(bang, ...)
function! rust_use#lib#commands#use(bang, ...)
    if a:bang == '!'
        call call('rust_use#use_new', a:000)
    else
        call call('rust_use#use', a:000)
    endif
endf

"'''''''''''''''''''' function! rust_use#lib#commands#use_path(bang, ...)
function! rust_use#lib#commands#use_path(bang, ...)
    if a:bang == '!'
        call call('rust_use#use_path_new', a:000)
    else
        call call('rust_use#use_path', a:000)
    endif
endf
