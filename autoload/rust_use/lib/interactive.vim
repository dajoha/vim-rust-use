
"'''''''''''''''''''' function! rust_use#lib#interactive#add_std_item(item, ...)
" Add a new use item, and try to group it with an existing use statement if possible.
function! rust_use#lib#interactive#add_std_item(item, ...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()

    " Check if the item is not already defined in a use statement:
    call rust_use#lib#use#check_item_defined(a:item)

    let item = rust_use#lib#interactive#find_std_item(a:item)
    if empty(item) | return | endif
    let path = rust_use#lib#path#split(item.path)
    call rust_use#lib#use#add_path(path, all_uses)
endf

"'''''''''''''''''''' function! rust_use#lib#interactive#add_std_item_new(item)
" Add a new use item, force the creation of a new use statement.
function! rust_use#lib#interactive#add_std_item_new(item, ...)
    let all_uses = a:0>0 ? a:1 : rust_use#lib#use#find_all_uses()

    " Check if the item is not already defined in a use statement:
    call rust_use#lib#use#check_item_defined(a:item)

    let item = rust_use#lib#interactive#find_std_item(a:item)
    if empty(item) | return | endif
    let path = rust_use#lib#path#split(item.path)
    call rust_use#lib#use#create_use_statement(path, all_uses)
endf

"'''''''''''''''''''' function! rust_use#lib#interactive#find_std_item(std_item)
function! rust_use#lib#interactive#find_std_item(std_item)
    " TODO: add option to allow automatic choice
    let items = rust_use#lib#std_items#find(a:std_item)
    if empty(items)
        throw printf("Unable to find identifier '%s'", a:std_item)
    endif
    if g:rust_use__detect_aliases && len(items) == 2
        let items = s:detect_alias(items)
    endif
    if len(items) == 1
        return items[0]
    else
        let message = 'Choose the path you want to add:'
        let item_list = map(copy(items), {k, item -> printf('%i. %s', k+1, item.path)})
        let choice = inputlist([ message ] + item_list)
        if choice == ''
            return {}
        endif
        if choice < 1 || choice > len(items)
            throw 'Bad input'
        endif
        return items[choice-1]
    endif
endf

"'''''''''''''''''''' function! rust_use#lib#interactive#ask_placement(places)
function! rust_use#lib#interactive#ask_placement(places)
    let nb_places = len(a:places)

    let choices = [ 'This item can be added at several places, please choose the place you want:' ]
    for i in range(nb_places)
        let place = a:places[i]
        let line = getline(place.use.start.l)
        if place.use.start.l != place.use.end.l
            let line .= ' [...]'
        endif
        let message = printf('%i. Line %i: %s', i+1, place.use.start.l, line)
        call add(choices, message)
    endfor
    call add(choices, printf('%i. Create a new use statement', nb_places + 1))

    let choice = inputlist(choices)

    if choice == ''
        return {}
    elseif choice < 1 || choice > nb_places + 1
        throw 'Bad input'
    elseif choice == nb_places + 1
        " TODO: smartly create the statement just after the last considered place, instead of
        " placing it after the last use.
        return 'force_creation'
    else
        return a:places[choice-1]
    endif
endf

"'''''''''''''''''''' function! s:detect_alias(items)
" Detect aliases: for example, if the items are:
"   - std::collections::hash_map::HashMap
"   - std::collections::HashMap
" Then choose automatically the short one.
" It works only when the second-last part of the longest item is the only difference.
" The given list must have a size of 2.
function! s:detect_alias(items)
    let item_short = a:items[0]
    let item_long = a:items[1]
    if strlen(item_short.path) > strlen(item_long.path)
        let [ item_short, item_long ] = [ item_long, item_short ]
    endif
    let path_short = rust_use#lib#path#split(item_short.path)
    let path_long = rust_use#lib#path#split(item_long.path)

    let head_len = len(path_short) - 1
    if path_short[:head_len-1] == path_long[:head_len-1]
        return [ item_short ]
    endif

    return a:items
endf
