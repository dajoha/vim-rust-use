
"'''''''''''''''''''' function! rust_use#use(...)
" Add a new use item, and try to group it with an existing use statement if possible.
function! rust_use#use(...)
    for item in a:000
        try
            call rust_use#lib#interactive#add_std_item(item)
        catch
            call rust_use#lib#util#show_exception()
        endtry
    endfor
endf

"'''''''''''''''''''' function! rust_use#use_new(...)
" Add a new use item, force the creation of a new use statement.
function! rust_use#use_new(...)
    for item in a:000
        try
            call rust_use#lib#interactive#add_std_item_new(item)
        catch
            call rust_use#lib#util#show_exception()
        endtry
    endfor
endf

"'''''''''''''''''''' function! rust_use#use_path(...)
" Add a new custom path, and try to group it with an existing use statement if possible.
function! rust_use#use_path(...)
    for path in map(copy(a:000), {k,v -> rust_use#lib#path#split(v)})
        try
            call rust_use#lib#use#add_path(path)
        catch
            call rust_use#lib#util#show_exception()
        endtry
    endfor
endf

"'''''''''''''''''''' function! rust_use#use_path_new(...)
" Add a new custom path, force the creation of a new use statement.
function! rust_use#use_path_new(...)
    for path in map(copy(a:000), {k,v -> rust_use#lib#path#split(v)})
        try
            call rust_use#lib#use#create_use_statement(path)
        catch
            call rust_use#lib#util#show_exception()
        endtry
    endfor
endf
